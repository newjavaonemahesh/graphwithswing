package e5d222;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.JComponent;

public class GraphComponent extends JComponent {
	
	private GraphModel graphModel;
	
	public GraphComponent(GraphModel graphModel) {
		this.graphModel = graphModel;
	}
	
/*	private void draw(Graphics2D g2) {
		
		int i = 0;
		double max = 0;

		ArrayList<Double> data = this.graphModel.getData();
		int width = this.graphModel.getWidth();
		int height = this.graphModel.getHeight();
		
		for (Double wrapper : data)
			if (max < wrapper)
				max = wrapper;

		int xwidth = width - 1;
		int yheight = height - 1;

		int xleft = 0;

		for (i = 0; i < data.size(); i++) {
			int xright = xwidth * (i + 1) / data.size();
			int barWidth = xwidth / data.size();
			int barHeight = (int) Math.round(yheight * data.get(i) / max);

			Rectangle bar = new Rectangle(xleft, yheight - barHeight, barWidth, barHeight);
			g2.draw(bar);

			xleft = xright;
		}
	}	

	*/
	
	private void draw(Graphics2D g2) {
		int i = 0;
		ArrayList<RectangleGeometry> data = this.graphModel.getGeometry();
		for (i = 0; i < data.size(); i++) {
			RectangleGeometry point = data.get(i);
			int xleft = point.getxLeft();
			int yTop = point.getyTop();
			int barWidth = point.getWidth();
			int barHeight = point.getHeight();
			Rectangle bar = new Rectangle(xleft, yTop, barWidth, barHeight);
			g2.draw(bar);
		}
	}	
	
	public void paintComponent(Graphics g)
	 {
		Graphics2D g2 = (Graphics2D) g;
		draw(g2);
	 }

}
