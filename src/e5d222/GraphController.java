package e5d222;

import java.awt.EventQueue;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JFrame;

public class GraphController extends JFrame implements ComponentListener, KeyListener {
	
	private GraphModel graphModel;
	private GraphInputView inputView;
	private GraphRendererView rendererView;
	
    private int width;
    private int height;
    private int numberOfPlots;
	
    public GraphController(int width, int height, int plots) {
    	this.width = width;
        this.height = height;
        this.numberOfPlots = plots;
        graphModel = new GraphModel(this.width, this.height);
        
        inputView = new GraphInputView(this, numberOfPlots);
        
        ArrayList<Double> graphValues = graphModel.getData(); 
        
        for (int i=0; i<numberOfPlots; i++) {
        	Double newValue = new Double(i+1);
        	graphValues.add(newValue);
        }
        
        graphModel.convertToRectGeometry();
        
        rendererView = new GraphRendererView(graphModel, this);
        
        inputView.addKeyListener(this);
        this.setTitle("InputView");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);        
        this.setContentPane(inputView);
        this.pack();
        this.setVisible(true);        
    }
    
    public void modifyInputViewAndModel(int currX, double delta) {
    	
    	if (delta>0) {
    		//subtract
    		System.out.println(Math.floor(delta));
    	} else {
    		System.out.println(Math.floor(delta));
    		//add
    	}
    	
    	Double prevX = Double.parseDouble(this.inputView.getGraphPlotText()[currX].getText());
    	
    	this.inputView.getGraphPlotText()[currX].setText(String.valueOf(prevX - delta));
    }
    
    public static void main(String [] args) {
        EventQueue.invokeLater(new Runnable() {
            //@Override
            public void run() {
            	new GraphController(Constants.FRAME_WIDTH, Constants.FRAME_HEIGHT, Constants.NO_OF_PLOTS);
            }
        });
    }
    
    public GraphModel getGraphModel() {
    	return this.graphModel;
    }

	@Override
	public void keyPressed(KeyEvent arg0) {
		System.out.println("keyEvent = " + arg0);
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentHidden(ComponentEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentMoved(ComponentEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentResized(ComponentEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentShown(ComponentEvent arg0) {
		// TODO Auto-generated method stub
		
	}    
	
}
