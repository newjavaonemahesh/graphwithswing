package e5d222;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class GraphRendererView extends JFrame implements Observer, MouseListener {

	private GraphModel graphModel;
	private GraphController graphController;
	private GraphComponent graphComponent;
	
	private JPanel content;

	public GraphRendererView(GraphModel graphModel, GraphController graphController) {
		this.graphModel = graphModel;
		this.graphModel.addObserver(this);
		this.graphController = graphController;

		this.setSize(graphModel.getWidth(), graphModel.getHeight());
		this.setTitle("GraphView");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		content = new JPanel();
		content.setLayout(new BorderLayout());
		content.setPreferredSize(new Dimension(700, 600));
		content.setOpaque(true);
		content.addMouseListener(this);
		
		this.graphComponent = new GraphComponent(graphModel);
		content.add(graphComponent);
		
		this.setContentPane(content);
		this.pack();
		this.setVisible(true);
	}

	@Override
	public void update(Observable object, Object arg1) {
		System.out.println("I am here");

		GraphModel model = (GraphModel) object;

		for (int i = 0; i < model.getData().size(); i++) {
			System.out.println(model.getData().get(i));
		}
		
		this.graphModel = model;
		
		this.graphModel.convertToRectGeometry();
		
		this.graphComponent.repaint();
		
		
		

	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		System.out.println("This is the event to handle");
		
		int x=arg0.getX();
	    int y=arg0.getY();
	    System.out.println(x+","+y);
	    
	    
		int i = 0;
		double max = 0;

		ArrayList<Double> data = this.graphModel.getData();
		int width = this.graphModel.getWidth();
		int height = this.graphModel.getHeight();
		
		for (Double wrapper : data)
			if (max < wrapper)
				max = wrapper;

		int xwidth = width - 1;
		int yheight = height - 1;

		int xleft = 0;
		double delta = 0.0;
		
		int currX = 0;
		for (i = 0; i < data.size(); i++,currX++) {
			int xright = xwidth * (i + 1) / data.size();
			xleft = xright;
			
			int barHeight = (int) Math.round(yheight * data.get(i) / max);
			
			if (xright < x) {
			  continue;
			} else {
				
				System.out.println("previousHeight - " + barHeight);
				System.out.println("currentHeight - " + (yheight-y));
				
				System.out.println("previousHeight - currentHeight = " + (barHeight - (yheight-y)));
				
				delta = (barHeight - (yheight-y))/(yheight/max);
				break;
			}
		}
		
		System.out.println("currX = " + currX);
		System.out.println("delta = " + delta);
		
		this.graphController.modifyInputViewAndModel(currX, delta);
	    
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
