package e5d222;

public class Constants {
    public static final int FRAME_WIDTH = 300;
    public static final int FRAME_HEIGHT = 400;
    
    public static final int NO_OF_PLOTS = 5;
    public static final double CONSTANT_VALUE = 5.0;
}
