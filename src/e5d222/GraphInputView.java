package e5d222;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;

public class GraphInputView extends JPanel implements DocumentListener  {

	private GraphModel graphModel;
	private GraphController graphController;
	
	private JTextField[] graphPlotText;
	
	public JTextField[] getGraphPlotText() {
		return graphPlotText;
	}


	public GraphInputView(GraphController graphController, int numberOfPlots) {
	    this.graphController = graphController;
	    this.graphPlotText = new JTextField[numberOfPlots];
	    
	    this.setLayout(new FlowLayout(FlowLayout.CENTER));
	    this.setPreferredSize(new Dimension(500, 300));
	    
	    for (int i=0; i<numberOfPlots; i++) {
	    	graphPlotText[i] = new JTextField();
	    	graphPlotText[i].setPreferredSize(new Dimension(50, 50));
	    	graphPlotText[i].getDocument().addDocumentListener(this);
	    	
	    	graphPlotText[i].getDocument().putProperty("id", i);
	    	this.add(graphPlotText[i]);
	    }
	}


	@Override
	public void changedUpdate(DocumentEvent arg0) {
		//Plain text components do not fire these events
		
	}

	@Override
	public void insertUpdate(DocumentEvent arg0) {
		modifyGraphModel(arg0, "inserted");
		
	}

	@Override
	public void removeUpdate(DocumentEvent arg0) {
		modifyGraphModel(arg0, "removed");
	}
	
	public void modifyGraphModel(DocumentEvent e, String action) {
        Document doc = (Document)e.getDocument();
        int changeLength = e.getLength();
        GraphModel model = graphController.getGraphModel();
        Integer id;
        String currentText="";
        
        ArrayList<Double> data;
        
        try {
	        System.out.println(
	            changeLength + " character" +
	            ((changeLength == 1) ? " " : "s ") +
	            action + " " + doc.getProperty("id") + "." + "\n" +
	            "  Text length = " + doc.getLength() + "\n");
	        
	        id = (Integer)doc.getProperty("id");
	        
	        if (doc.getLength()>0) {
	          currentText = doc.getText(0, doc.getLength());
	        } else if (doc.getLength()==0) {
	          currentText = "0";
	        }
	        
	        model.changeData(id, currentText);
	        
        } catch (Exception exp) {
        	exp.printStackTrace();
        }
        
    }	
	
	
	
	
	
}
