package e5d222;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Observable;

public class GraphModel extends Observable {

	private int width;
	private int height;
	private ArrayList<Double> data;
	
	private ArrayList<RectangleGeometry> geometry;
	
	public GraphModel(int width, int height) {
		super();
		this.width = width;
		this.height = height;
		data = new ArrayList<Double>();
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public ArrayList<Double> getData() {
		return data;
	}

	public void setData(ArrayList<Double> data) {
		this.data = data;
	}
	
	public ArrayList<RectangleGeometry> getGeometry() {
		return geometry;
	}

	public void setGeometry(ArrayList<RectangleGeometry> geometry) {
		this.geometry = geometry;
	}

	void changeData(Integer id, String data) {
		this.getData().set(id, Double.parseDouble(data));
		
        setChanged(); // the two methods of Observable class
        notifyObservers(this);
    }
	
	void convertToRectGeometry() {
		ArrayList<Double> data = this.getData();
		int width = this.getWidth();
		int height = this.getHeight();
		
		int i = 0;
		double max = 0;
		
		geometry = new ArrayList<RectangleGeometry>();
		
		for (Double wrapper : data)
			if (max < wrapper)
				max = wrapper;

		int xwidth = width - 1;
		int yheight = height - 1;

		int xleft = 0;

		for (i = 0; i < data.size(); i++) {
			int xright = xwidth * (i + 1) / data.size();
			int barWidth = xwidth / data.size();
			int barHeight = (int) Math.round(yheight * data.get(i) / max);

			geometry.add(new RectangleGeometry(xleft, yheight - barHeight, barWidth, barHeight));
			
			xleft = xright;
		}		
	}

}
