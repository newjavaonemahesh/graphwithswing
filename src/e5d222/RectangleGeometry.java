package e5d222;

public class RectangleGeometry {
	
	private int xLeft;
	private int yTop;
	private int width;
	private int height;
	
	public RectangleGeometry(int x, int y, int w, int h) {
		this.xLeft = x;
		this.yTop = y;
		this.width = w;
		this.height = h;
	}
	
	public int getxLeft() {
		return xLeft;
	}
	public void setxLeft(int xLeft) {
		this.xLeft = xLeft;
	}
	public int getyTop() {
		return yTop;
	}
	public void setyTop(int yTop) {
		this.yTop = yTop;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
}
